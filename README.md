## This is the test application made by Pablo Marcos for the Newstore selection process.

Here are the steps to follow to run this app:

1. npm (8.1.2), node.js (v16.13.2) and the @angular/cli@12 package are required to run this project.
2. Clone this repository. The branch named 'main' has all the code.
3. Run the following command:

```
npm install 
```

4. To run the application, just run:
```
ng serve 
```

5. To open the app, just type localhost:4200 on any navigator while 'ng serve' is running.

## TESTING
Functional tests are located in "src\app\pages\main\main.component.spec.ts".

To run the tests, just run the following command:
```
ng serve --no-watch --code-coverage
```