import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GithubService } from 'src/app/services/github.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  public name: string = '';
  public id: number = 0;
  public avatarId: string = '';
  public followers: number = 0;
  public following: number = 0;
  public stars: number = 0;
  public twitterUsername: string = '';

  public organizations: Organization[] = [];
  public repos: Repo[] = [];

  constructor(
    private readonly githubService: GithubService,
    private readonly activatedRoute: ActivatedRoute,
    private readonly router: Router,
    ) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(params => {

      this.githubService.getUserFromGithub(params.userid).subscribe(userData => {
        this.name = userData.login;
        this.id = userData.id;
        this.avatarId = userData.avatar_url;
        this.followers = userData.followers;
        this.following = userData.following;
        this.twitterUsername = userData.twitter_username;
      }, () => {
        this.router.navigate(['404']);
      });

      this.githubService.getUserOrganizations(params.userid).subscribe(organizations => {
        this.organizations = organizations;
      }, () => {
        this.router.navigate(['404']);
      });

      this.githubService.getUserRepos(params.userid).subscribe(repositories => {
        this.repos = repositories;
      }, () => {
        this.router.navigate(['404']);
      });

      this.githubService.getStarredRepos(params.userid).subscribe(repositories => {
        this.stars = repositories.length;
      }, () => {
        this.router.navigate(['404']);
      });
    });
  }

}
