import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { MainComponent } from './main.component';

describe('MainComponent', () => {
  let component: MainComponent;
  let fixture: ComponentFixture<MainComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MainComponent ],
      imports: [ RouterTestingModule ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should go to user\'s page if the search input has three or more characters', async () => {
    component.username = '123';
    const navigateSpy = spyOn(component.router, 'navigate');
    component.goToUserPage();
    expect(navigateSpy).toHaveBeenCalledWith(['user/123']);
  });

  it('should not go to user\'s page if the search input has less than three characters', async () => {
    component.username = '12';
    const navigateSpy = spyOn(component.router, 'navigate');
    component.goToUserPage();
    expect(navigateSpy).toHaveBeenCalledTimes(0);
  });
});
