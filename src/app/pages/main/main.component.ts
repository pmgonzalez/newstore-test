import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  public username: string = '';

  constructor(public readonly router: Router) {}

  ngOnInit(): void {
  }

  goToUserPage() {
    this.username.length >= 3 && this.router.navigate([`user/${this.username}`]);
  }
}
