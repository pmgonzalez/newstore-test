interface GithubUserInterface {
    login: string;
    id: number;
    avatar_url: string;
    followers: number;
    following: number;
    stars: string;
    twitter_username: string;
}

interface Organization {
    login: string;
    avatar_url: string;
}

interface Repo {
    full_name: string;
    description: string;
    forks_count: number;
    stargazers_count: number;
    language: string;
    pushed_at: string;
}