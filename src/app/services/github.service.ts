import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GithubService {
  private baseURL = 'https://api.github.com/users/';
  constructor(private readonly http: HttpClient) { }

  getUserFromGithub(user: string) {
    return this.http.get(`${this.baseURL}${user}`) as Observable<GithubUserInterface>;
  }

  getUserOrganizations(user: string) {
    return this.http.get(`${this.baseURL}${user}/orgs?per_page=10&page=1`) as Observable<Organization[]>;
  }

  getUserRepos(user: string) {
    return this.http.get(`${this.baseURL}${user}/repos?sort=pushed&direction=desc&per_page=6&page=1`) as Observable<Repo[]>;
  }

  getStarredRepos(user: string) {
    return this.http.get(`${this.baseURL}${user}/starred`) as Observable<Repo[]>;
  }
}
